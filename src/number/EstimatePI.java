package number;

import java.util.Random;

public class EstimatePI {
	/*
	 * 		   ^ Y
	 *         |
	 *       ..|..
	 *      .  |   . x^2 + y^2 = 0.5^2
	 *- -----------.---------->
	 *      .  |   .		X
	 *       ..|. .
	 *         |
	 *       
	 * 
	 * 
	 * 
	 * 
	 */
	public static double PI(int num){
//		int PI=0;
		double count=0;// point drop in circle
		final int TRIAL = num;
		double x=0, y=0;
//		Random r = new Random(System.currentTimeMillis());
		Random r = new Random();
		while(num-- > 0){
			x=r.nextDouble() - 0.5;
			y=r.nextDouble() - 0.5;
			if(x*x + y*y <= 0.25)
				count ++;
				
		}
		// pi*r^2 / 1 = count / num   ----> pi = 4 * (count / num) 
//		System.out.println(count);
		return 4.0 * (count / TRIAL);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	/*	Random r = new Random(System.currentTimeMillis());
		System.out.println(r.nextLong());
		for(int i=0; i<10; i++)
		System.out.println(r.nextDouble());
		*/
		System.out.println(PI(1000000));
		/*
		 * 3.14456
		 * 3.143388
		 * 3.138768

		 */
	}

}
