package number;

/*
 * greatest common divisor
 */
public class GCD {
/*	public static int getGCD(int a, int b){
		int tmp;
		// make a >b
		if(a < b){
			tmp = a;
			b = a;
			a = tmp;
		}
		
		while(a !=0 ){
			if(a<b){
				tmp = a;
				b = a;
				a = tmp;
			}
			a = a%b;
		}
		
		return b;
	}*/
	
	
	public static int gcd(int a, int b){
		return b == 0? a : gcd(b, a%b);
		
	}
}
