package string;

import array.sort.Utils;

public class KMP {
	
	/*
	 * refer
	 * http://www.ruanyifeng.com/blog/2013/05/Knuth%E2%80%93Morris%E2%80%93Pratt_algorithm.html
	 * http://jakeboxer.com/blog/archives/
	 * 
	 */
	
	public static int[] partialMatchTable(String arr){
		return partialMatchTable(arr.toCharArray());
	}
	
	public static int[] partialMatchTable(char[] w){
		int[] table = new int[w.length];
		
		int len = table.length;
		
		/*         i
		 *		 /	 
		 *   [] [] [] [] [] 
		 *   /
		 *   j
		 *   
		 */ 
		int j=0;
		for(int i = 1; i<len; i++ ){
			while(j>0 && w[i] != w[j])
				j=table[j-1];
			if(w[i] == w[j])
				j++;
			table[i] = j;
		}
		
		return table;
	}
	
	/*
	 * find substring(pattern) in a giving string
	 */
	public static int KMPMatch(String sub, String str){
		char[] p = sub.toCharArray();
		char[] s = str.toCharArray();
		int[] table = partialMatchTable(p);
		Utils.p(table);
		/*
		 * i -> p pattern
		 * j -> s  text
		 */
//		int breakOut = 0;
//		int m = s.length * p.length;
		int i=0, j=0;
		while( j < s.length && i < p.length){
			System.out.print(" " + "[" + i + "," + j +"]");
			if(p[i] == s[j]){
				i ++;
				j ++;
			}else{
				if(i>0)
					i = i - (i-table[i-1]);
				else{
					i = 0;
					j ++;
				}
			}	
			
/*			if(breakOut++ > m){
				System.out.println("break ...");
				break;
			}*/
		}
		System.out.println("");
		if(i == p.length){
			return j-i;
		}
		return -1;
	}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		Utils.p(partialMatchTable("ABAB"));
		String w = "aaac";
		String s = "aaaabacaabac";
		Utils.p( KMPMatch(w, s) );
	}

}
