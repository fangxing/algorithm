package string;

import array.sort.Utils;

public class BinarySearch {
	
	/*
	 * binary search in recursive
	 * 
	 * @start start index
	 * @end end index
	 * @arr find in arr array
	 * @object 
	 */
	public static int binarySearch(int start, int end, int[] arr, int object){
		if(start > end) return -1;
		
		int mid = start + (end - start) / 2;
		
		if(object == arr[mid]) return mid;
		
		if(object <= arr[mid]){
			return binarySearch(start, mid-1, arr, object);
		}else{
			return binarySearch(mid+1, end, arr, object);
		}
	}
	
	/*
	 * binary search in loop
	 */
	
	public static int binarySearch(int[] arr, int object){
		int mid;
		int start=0, end=arr.length-1;
		while(start <= end){
			mid = start + (end - start) / 2; // (start + end) / 2
			if(object < arr[mid]){
				end = mid-1;
			}else if(object > arr[mid]){
				start = mid+1;
			}else{
				return mid;
			}
			
		}
		return -1;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = {1,2,3,4,4,4,4,5,5,6,7,8,9};
		Utils.p(binarySearch(arr, 7));
	}

}
