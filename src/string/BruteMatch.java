package string;

public class BruteMatch {
	
	/* 暴力匹配
	 * return index of first time a word match in a string i
	 */
	public static int match(String word, String str){
		if(word.length() > str.length()) return -1;
		char w[] = word.toCharArray();
		char s[] = str.toCharArray(); 
		
		int i=0, j=0; 
		while(i<word.length() && j<str.length()){
			/* 
			 * 如果当前字符相同, 则继续向前匹配
			 * 否则 i=0, j=j-(i+1) 从新从匹配
			*/
			if(w[i] == s[j]){
				i++;
				j++;
			}else{
				i = 0;
				j = j - i + 1;
			}
		}
		if(i == word.length()){
			return j - i;
		}
		return -1;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String w = "aaac";
		String s = "aaaabacaaac";
		System.out.println(match(w, s));
	}

}
