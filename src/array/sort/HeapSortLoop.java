package array.sort;

import java.util.Arrays;

public class HeapSortLoop {
	
	public static void heaptify(int[] arr, int heapSize, int index){
		int iMax; // = index;
		int iLeft; // 2*index + 1;
		int iRight; // 2*index + 2;
		while(true){
			iMax = index;
			iLeft = 2*index + 1;
			iRight = 2*index + 2;
			if(iLeft < heapSize && arr[iLeft] > arr[iMax]) {
				iMax = iLeft;
			}
			if(iRight < heapSize && arr[iRight] > arr[iMax])
				iMax = iRight;
			
			if(iMax != index){
				swap(arr, index, iMax);
				
				index = iMax;
			}else{
				break;
			}
		}	
	}
	
	static void buildHeap(int[] arr, int heapSize){
		for(int i=(heapSize-1)/2; i>=0; i--){
			heaptify(arr, heapSize, i);
		}
	}
	
	static void heapSort(int[] arr){
		
		for(int i=arr.length; i>0; i--){
			buildHeap(arr, i);
			swap(arr, 0, i-1);
		}
	}
	
	public static void swap(int[] arr, int i, int j){
		int tmp = arr[i];
		arr[i] = arr[j];
		arr[j] = tmp;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int arr[] = {1,5,2,4,1,4,7,2,5,9,6,2,8,9};
		heapSort(arr);
		System.out.println(Arrays.toString(arr));
	}

}
