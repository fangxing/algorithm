package array.sort;

import java.util.Arrays;

public class BubbleSort {
	public static int[] bubbleSort(int[] arr) {
		int tmp = 0;
		for (int j = arr.length - 1 ; j > 0; j--) {
			for (int i = 0; i < j; i++) {
				if (arr[i] > arr[i + 1]) {
					tmp = arr[i];
					arr[i] = arr[i + 1];
					arr[i + 1] = tmp;
				}

			}

		}
		return arr;

	}
	
	public static int[] selectSort(int[] arr){
		int maxIndex = 0;
		int tmp = 0;
		for(int i=arr.length-1; i>0; i--){
			maxIndex = 0;
			for(int j=1; j <= i; j++){
				if(arr[j] > arr[maxIndex]){
					maxIndex = j;
				}
			}
			tmp = arr[i];
			arr[i] = arr[maxIndex];
			arr[maxIndex] = tmp;
		}
		return arr;
	}
	
	/* reference
	 * https://www.youtube.com/watch?v=DFG-XuyPYUQ
	 */
	public static int[] insertSort(int[] arr){
	// sorted	  unsorted
	//{ 1  | ,4,2,3,6,12,3
		int tmp = 0;
		for(int i=1; i<arr.length; i++){
			tmp = arr[i];
			int j=i;
			// move element to empty a location
			while(j>0 && arr[j-1] > tmp){
				arr[j] = arr[j-1];
				j --;
			}
			arr[j] = tmp;
		}
		return arr;
		
	}

	public static void main(String[] args) {
		int[] arr = {1,4,2,3,6,12,3};
//		bubbleSort(arr);
//		selectSort(arr);
		insertSort(arr);
		System.out.print(Arrays.toString(arr));

	}

}
