package array.sort;

import java.util.Arrays;

public class MergeSort {

	public static void mergeSort(int arr[]){
		if(arr == null || arr.length < 2) return;
		
		int len = arr.length;
		int mid = len/2;
		int leftLen = mid;
		int rightLen = len - leftLen;
		
		
		/*
		 * int[] left = Arrays.copyOfRange(inputArray, 0, leftSize); int[] right
		 * = Arrays.copyOfRange(inputArray, leftSize, inputArray.length);
		 */
		
		/* 
		 int[] left = new int[leftLen];
		 int[] right = new int[rightLen];
		 for(int i=0; i<mid; i++){
			left[i] = arr[i];
		}
		for(int i=mid; i<len; i++){
			right[i-mid]=arr[i];
		}*/
		
		int[] left = Arrays.copyOfRange(arr, 0, leftLen);
		int[] right = Arrays.copyOfRange(arr, mid, arr.length);
		
		mergeSort(left);
		mergeSort(right);
		mergeArray(left, right, arr);
	}
	
	public static void mergeArray(int[] a, int[] b, int[] c){
//		int[] c = new int[a.length + b.length];
		int i=0, j=0, k=0;
		while(i < a.length && j < b.length){
			if(a[i] > b[j]){
				c[k] = b[j];
				j++;
			}else{
				c[k] = a[i];
				i++;
			}
			k++;
		}
//		System.out.println(Arrays.toString(c));
		while(i<a.length){
			c[k++] = a[i];
			i ++;
		}
		while(j<b.length){
			c[k++] = b[j];
			j ++;
		}
		System.out.println(Arrays.toString(c));
//		return c;
	}
	
/* stackoverflow
 * I'm surprised no one has mentioned this much more cool, efficient and compact implementation:
*/
	public static int[] merge(int[] a, int[] b) {
	    int[] answer = new int[a.length + b.length];
	    int i = a.length - 1, j = b.length - 1, k = answer.length;

	    while (k > 0)
	        answer[--k] = 
	            (j < 0 || (i >= 0 && a[i] >= b[j])) ? a[i--] : b[j--];
	    return answer;
	}
	/*
	 * Points of Interests

	Notice that it does same or less number of operations as any other O(n) algorithm but in literally single statement in a single while loop!
	If two arrays are of approximately same size then constant for O(n) is same. However if arrays are really imbalanced then versions with System.arraycopy would win because internally it can do this with single x86 assembly instruction.
	Notice a[i] >= b[j] instead of a[i] > b[j]. This guarantees "stability" that is defined as when elements of a and b are equal, we want elements from a before b.
	
	
	 * merge to sorted array in traditional way
	 */
	public static int[] mergeArray(int[] a, int[] b){
		int[] c = new int[a.length + b.length];
		int i=0, j=0, k=0;
		while(i < a.length && j < b.length){
			if(a[i] > b[j]){
				c[k] = b[j];
				j++;
			}else{
				c[k] = a[i];
				i++;
			}
			k++;
		}
//		System.out.println(Arrays.toString(c));
		while(i<a.length){
			c[k++] = a[i];
			i ++;
		}
		while(j<b.length){
			c[k++] = b[j];
			j ++;
		}
		
		return c;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a = {2,5,6,9,11};
		int[] b = {1,3,5,7,8,9,9,10};
		int[] c = {3,1,2,4,3,2,6,8,3,7,22,3,45,6,7};
		int[] d = new int[a.length+b.length];
//		mergeArray(a, b, d);
//		a.length = 2;
//		Arrays.toString(a);
		mergeSort(c);
		System.out.println(Arrays.toString(c));

	}

}
