package array.sort;

import java.util.Arrays;

public class QuickSort {

	public static int partion(int start, int end, int[] arr){
		int pivotValue = arr[end-1];
		int storeIndex = start; // store value less than pivot
		/*
		 * storeIndex = start
		 * 	\	
		 *  [] []  []  [] []
		 *   \         |    \
		 *   i		  end-1	 pivot
		 */  
		
		for(int i=start; i < end-1; i++){
			if(arr[i] <= pivotValue){
				Utils.swap(arr, storeIndex, i);
				storeIndex ++;
			}
		}
		// swap pivot to center
		Utils.swap(arr, storeIndex, end-1);
		return storeIndex;
	}
	
	
	public static void quickSort(int start, int end, int[] arr){
		// 终止条件
		if(start >= end) return;
		int pivotIndex = partion(start, end, arr);
		
		// 递归
		quickSort(start, pivotIndex, arr);
		quickSort(pivotIndex+1, end, arr);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] arr = {2,3,4,5,7,3,5,1,0,22};
		quickSort(0, arr.length, arr);
		System.out.println(Arrays.toString(arr));
		
	}

}
