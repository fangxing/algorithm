package array.sort;

import java.util.Arrays;

public class InsertSort {

	public static void insertSort(int[] arr){
		for(int i=1; i<arr.length; i++){
			int tmp = arr[i];
			int j = i-1;
			while(j>=0 && arr[j] > tmp){
				arr[j+1] = arr[j];
				j--;
			}
			arr[j+1] = tmp;
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = {3,1,2,6,3,8,7};
		insertSort(arr);
		System.out.println(Arrays.toString(arr));
	}

}
