package array.sort;

import java.util.Arrays;

public class HeapSort {
	
	/*  reference http://bubkoo.com/2014/01/14/sort-algorithm/heap-sort/
	 *            https://stackoverflow.com/questions/8938375/an-intuitive-understanding-of-heapsort
	 *	Parent(i) = floor((i-1)/2)，i 的父节点下标
		Left(i) = 2i + 1，i 的左子节点下标
		Right(i) = 2i + 2，i 的右子节点下标
	 */

	public static void heaptify(int[] arr, int index, int heapSize){
		int iMax = index; // 首先假设index是最大的
		int iLeft = 2*index + 1;
		int iRight = 2*index + 2;

		/*
		 *         2(index, iMax)						     2				  2
		 *       /    \             --->iMax=iLeft         /    \    -->    /   \ 
		 *     3(iLeft) 5(iRight)				        3(iMax)  5         3     5 (iMax)
		 * 
		 */
		// 不能 iLeft <= heapSize, 因为heapSize(从1开始) = array.length (零开始)
		if(iLeft < heapSize && arr[index] < arr[iLeft]){
			iMax = iLeft;
		}
		
		if(iRight < heapSize && arr[iMax] < arr[iRight]){
			iMax = iRight;
		}
		
		// swap 
		if(index != iMax){
			swap(arr, index, iMax); // 使index成为最大元素
			//recursive
			heaptify(arr, iMax, heapSize);  // 递归调整
		}
	}
	
	public static void buildHeap(int[] arr, int heapSize){
		int iParent = (heapSize-1)/2; // array_size 是数组长度下表从1开始, 而堆是从1开始所以减去1
		
		for(int i=iParent; i>=0; i--){
			heaptify(arr, i, heapSize);
		}
	}
	
	public static void heapSort(int[] arr){
//		buildHeap(arr, arr.length);
		for(int i=arr.length; i>0; i--){
			buildHeap(arr, i);
			swap(arr,0, i-1);
//			buildHeap(arr, i);
		}
	}
	
	public static void swap(int[] arr, int i, int j){
		int tmp = arr[i];
		arr[i] = arr[j];
		arr[j] = tmp;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int arr[] = {2,4,6,1,3,6,8};
		heapSort(arr);
		System.out.println(Arrays.toString(arr));
	}
}
