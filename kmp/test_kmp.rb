#!usr/bin/env ruby
#
require 'minitest/autorun'
require_relative "./kmp.rb"

class KMP_test < Minitest::Test

  def test_jump_function
    pattern = "ababa"
    jump = [-1, 0, 0, 1, 2]
    assert_equal jump, KMP.overlap(pattern)

    pattern = "abcd"
    jump = [-1, 0, 0, 0]
    assert_equal jump, KMP.overlap(pattern)

  end

  def test_kmp_match
    target = "ababababa"
    i = KMP.match target, "aba"
    assert_equal 0, i

    i = KMP.match target, "abc"
    assert_equal -1, i

    i = KMP.match target, "baba"
    assert_equal 1, i

  end
end
