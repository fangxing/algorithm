#!/usr/bin/env ruby


class KMP

  class << self

    #  index   0  1  2  3  4  5  6  7  8  9  10 11 12 13
    # target   A  B  C  A  B  C  B  B  C  A  B  C  A  B
    #                            ^
    #                            i
    # pattern  A  B  C  A  B  C  A  A  B
    #                            j
    # jump     0  0  0  1  2  3  1  1  2
    #

    # 在j=6的时候失配， 从图中看出， 我们想跳到index=4; jump[j-1]=jump[5]=3, j = j - jump[j-1] + 1
    # 由于j-1可能在低处越界, 所以要判断一下

    def match(target, pattern)
      return false if target.nil? || target.size < 1
      jump  = overlap(pattern)
      puts "jump: #{jump}"
      #result = []
      i, j = 0, 0
      while i < target.size && j < pattern.size
        #puts "#{i}, #{j}"
        if j==-1 || pattern[j] == target[i]
          i += 1
          j += 1
        else
          j = jump[j]
        end
      end

      if j == pattern.size # i 已经超出边界, 说明全部匹配到了
         i - j
      else
        -1
      end
    end

    def overlap(pattern)
      # 计算overlap
      # Example:
      # pattern =
      # abaab
      # ^ ^
      # i j
      #######

      i = -1
      j = 0
      jump = [-1] # 表示移动位数， 由于在0位置不能在右移， -1表是左移
      # 时间复杂度O(m)
      while j < pattern.size - 1
        if i == -1 ||  pattern[i] == pattern[j]
          i += 1
          j += 1
          jump[j] = i
        else
          i = jump[i] # 递归
        end
      end

      jump
    end


  end

end # end KMP
