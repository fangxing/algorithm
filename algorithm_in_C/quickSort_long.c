#include <stdio.h>

void swap();

void swap(int *a, int *b){
//  printf("a=%d, b=%d", *a, *b);
  int tmp = *a;
  *a = *b;
  *b = tmp;
}

int partion(int arr[], int start, int end){
    int pivot = arr[end];
    int storeIndex = start;
    int i = start;
    while(i < end){
      if(arr[i] < pivot){
        swap(&arr[storeIndex], &arr[i]);
        storeIndex ++;
      }
      i ++;
    }
    swap(&arr[storeIndex], &arr[end]);
    return storeIndex;
}
/*
 * start: first array index
 * end:   last array index
 */
void quickSort(int arr[], int start, int end){
  if(start >= end)  return; 
  int pivot = partion(arr, start, end);
  
  quickSort(arr, start,  pivot-1);
  quickSort(arr, pivot + 1, end);
}
void show(int arr[],int  len){
  int i=0;
  while(i<len){
    printf("%d ", arr[i++]);
  }
  printf("\n");
  
}
int main(){
  //int arr[] = {3,6,2,1,5};
  int arr[] = {2,4,9,7,3,1,5, 0, 22,33,1,2,4,5};
  int len = 14;
  quickSort(arr, 0, len-1);
  show(arr, len);
  return 0;
}
