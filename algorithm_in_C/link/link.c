#include<stdio.h>
#include<stdlib.h>

/*
 * link list in c
 *
 * addLast 
 * addFirst
 * delete(before, current, after))
 * display
 * exchange
 * find
 * free list
 * insert(before, after)
 * init(head, after)
 * isEmpty
 * modify
 * reverse
 * sort
 */

/*
 * define link node
 */
typedef struct node{
  int data;
  struct node *next;
} node;

int addLast(node *head, int x){
  return 0;
}
void display(node *head){
  node *p = head;
  while(p){
    printf("%d ", p->data);
    p = p->next;
  }
  printf("");
}
node *initFromArray(node *head, int arr[]){
    int i, len = sizeof(arr)/sizeof(arr[0]);
    node *p = head = malloc(sizeof(node)); 
    p->data = 1;
    for(i=0; i < len; i++){
      p->next = malloc(sizeof(node));
      p = p->next;
      p->data = arr[i];
    }
    return head;
}

int main(){
  node *head = NULL;
  int arr[] = {1,2,3,4,5,6};
  initFromArray(&head, arr);
  display(&head);
}
