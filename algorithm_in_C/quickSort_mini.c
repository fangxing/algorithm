#include <stdio.h>
/*
 *      left           right
 *  ... [] [] [] [] [] []
 *      storeIndex      pivot
 *
 *      storeIndex 用来分隔小于pivot的元素
 *
 *   参考视频   https://www.youtube.com/watch?v=aQiWF4E8flQ
 */
void swap();
void quickSort(int arr[], int left, int right){
    if(left > right) return;
    int storeIndex = left, pivot = arr[right], j;
    for(j = left; j < right; j ++) if(arr[j] < pivot) swap(arr, storeIndex ++, j);
    swap(arr, storeIndex, right);
    quickSort(arr,left, storeIndex-1);
    quickSort(arr, storeIndex+1, right);
}

void swap(int arr[], int i, int j){
  if(i == j) return;
  int tmp = arr[i];
  arr[i] = arr[j];
  arr[j] = tmp;
}

void show(int arr[], int len){
  int i = 0;
  while(i < len)
    printf("%d ", arr[i++]);
  printf("\n");
}
int main(){
  int arr[] = {2,4,9,7,3,1,5, 0, 22,33,1,2,4,5}; //len=12
  int len = 14;
  quickSort(arr, 0, len - 1);
  show(arr, len);
}
